﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Countdown_Letters_Game
{
    /// <summary>
    /// A console version of the popular Countdown Letters game. 
    /// Only allows British spellings.
    /// Finds the longest word each round.
    /// </summary>
    class Program
    {
        static readonly string dictionaryFilename = "words_british.txt";
        static readonly string anagramDictionaryFilename = "anagrams.txt";
        static readonly string frequenciesFilename = "LetterFrequencies.txt";

        static readonly int[] primes = new int[] { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107 };
        static readonly HashSet<char> alphabet = new HashSet<char>("qwertyuiopasdfghjklzxcvbnm".ToCharArray());
        static readonly HashSet<char> consonants = new HashSet<char>("qwrtpsdfghjklzxcvbnmy".ToCharArray());
        static readonly HashSet<char> vowels = new HashSet<char>("aeiou".ToCharArray());

        static readonly HashSet<string> acceptedInputsMeaningConsonant = new HashSet<string>() { "c", "C", "Consonant", "consonant", "CONSONANT" };
        static readonly HashSet<string> acceptedInputsMeaningVowel = new HashSet<string>() { "v", "V", "Vowel", "vowel", "VOWEL" };
        static readonly HashSet<string> acceptedInputsMeaningYes = new HashSet<string>() { "y", "Y", "Yes", "yes", "YES" };
        static readonly HashSet<string> acceptedInputsMeaningNo = new HashSet<string>() { "n", "N", "No", "no", "NO" };

        static readonly int numRounds = 4;
        static readonly int maxWordLength = 9;

        static float consonantFrequencySum = 0;
        static float vowelFrequencySum = 0;


        static void Main(string[] args)
        {
            string rootAddress = GetRootAddress();
            Dictionary<char, float> letterFrequencies = ReadLetterFrequencies(rootAddress);
            Dictionary<char, int> charHashes = GenerateCharHashes();
            Dictionary<double, List<string>> AnagramDictionary = ReadAnagramFile(rootAddress, charHashes);

            bool wantsToPlay = GetBooleanResponse("Would you like to play the Countdown Letters Game? (y/n)",
                                        "I didn't understand that. Please type y or n: ",
                                        acceptedInputsMeaningYes, acceptedInputsMeaningNo);
            while (wantsToPlay)
            {
                PlayGame(letterFrequencies, charHashes, AnagramDictionary);
                wantsToPlay = GetBooleanResponse("Would you like to play again? (y/n)",
                                        "I didn't understand that. Please type y or n: ",
                                        acceptedInputsMeaningYes, acceptedInputsMeaningNo);

            }

            Console.WriteLine("Goodbye");
        }

        /// <summary>
        /// Plays four rounds.
        /// A round goes like this:
        /// -Generates nine letters, getting input from player to decide consonant or vowel
        /// -Gets a word from the player (if they have one)
        /// -Declares points
        /// -Declares longest possible word for these letters
        /// </summary>
        static void PlayGame(Dictionary<char, float> letterFrequencies, Dictionary<char, int> charHashes, Dictionary<double, List<string>> AnagramDictionary)
        {
            int score = 0;
            string[] roundNames = new string[] { "ONE", "TWO", "THREE", "FOUR"};

            //Intro text
            Console.WriteLine("\nWelcome to the Countdown Letters Game\n");

            for (int round=0; round < numRounds; round++)
            {
                Console.WriteLine("ROUND " + roundNames[round] + "\n");

                //Ask user for consonant or vowel (nine times)
                string longestWord = "";
                string letters = "";
                while (longestWord == "")
                {
                    letters = GetLetters(letterFrequencies);

                    longestWord = FindLongestWord(letters, charHashes, AnagramDictionary);
                    if (longestWord == "")
                    {
                        Console.WriteLine("\nNo word can be made from these letters! Choose letters again.\n");
                    }
                }
                Console.WriteLine("Your letters are: " + letters);
                //Once nine letters chosen, get users word
                string word = GetWord(letters, charHashes, AnagramDictionary);
                score = GetWordScore(word, score);
                if (longestWord.Length == word.Length)
                {
                    Console.WriteLine("You got the longest word!");
                } 
                else
                {
                    Console.WriteLine("Longest word is " + longestWord);
                }
            }

            Console.WriteLine("Thanks for playing the Countdown Letters Game. Your final score is " + score + "\n\n");
        }

        /// <summary>
        /// Score is the length of the word, except it is doubled for words of maximum length.
        /// Also prints a message announcing score
        /// </summary>
        static int GetWordScore(string word, int score)
        {
            int wordPoints = word.Length;
            if (wordPoints > 0)
            {
                if (wordPoints == maxWordLength)
                {
                    score += wordPoints;
                    Console.WriteLine("Wow! Great Job!");
                }
                score += wordPoints;
                Console.WriteLine("Your word is worth " + wordPoints + " points. Your total score is now " + score);
            }
            else
            {
                Console.WriteLine("No points. Your total score is still " + score);
            }
            return score;
        }

        /// <summary>
        /// Gets user input to get a word. It must be:
        /// -made of the right letters
        /// -a word in the dictionary
        /// -not empty
        /// </summary>
        static string GetWord(string letters, Dictionary<char, int> charHashes, Dictionary<double, List<string>> AnagramDictionary)
        {
            string input;
            bool finishedGettingInput = false;
            string message;
            while (!finishedGettingInput)
            {
                Console.WriteLine("Enter your word: ");
                input = Regex.Replace(Console.ReadLine(), @"\t|\n|\r", "");

                if (input.Length == 0)
                {
                    message = "You didn't enter anything. Do you want to enter a word? (y/n)";
                } 
                else
                {
                    double hash = TryHash(input, charHashes);
                    double lettersHash = Hash(letters, charHashes);
                    if (hash == -1)
                    {
                        message = "Your response contained characters which aren't letters. Do you want to enter a word? (y/n)";
                    }
                    else if (lettersHash / hash % 1 != 0)
                    {
                        string invalidLetters = RemoveStringIntersection(letters, input);
                        message = "Your word contained the letters " + invalidLetters + ", which aren't in " + letters + ". Do you want to enter another word? (y/n)";
                    }
                    else if (!(AnagramDictionary.ContainsKey(hash) && AnagramDictionary[hash].Contains(input)))
                    {
                        message = "Your word is not in our dictionary. Do you want to enter another word? (y/n)";
                    }
                    else
                    {
                        return input;
                    }
                }
                finishedGettingInput = !GetBooleanResponse(message, "I didn't understand that. Please type y or n: ",
                                                            acceptedInputsMeaningYes, acceptedInputsMeaningNo);
            }
            return "";
        }

        /// <summary>
        /// Returns the result of removing the intersection of both strings from the second string
        /// </summary>
        static string RemoveStringIntersection(string string1, string string2)
        {
            List<char> list1 = new List<char>(string1.ToCharArray());
            List<char> list2 = new List<char>(string2.ToCharArray());
            List<char> output = new List<char>();

            foreach (char c in list2)
            {
                if (list1.Contains(c))
                {
                    list1.Remove(c);
                } 
                else
                {
                    output.Add(c);
                }
            }

            return new string(output.ToArray());
        }

        /// <summary>
        /// Returns the longest word which can be made from the letters given.
        /// </summary>
        static string FindLongestWord(string letters, Dictionary<char, int> charHashes, Dictionary<double, List<string>> AnagramDictionary)
        {
            string word = "";
            double hash;
            HashSet<char> letterSet;

            HashSet<string> subsetsOfLetters = new HashSet<string> { letters };
            HashSet<string> nextSubSets = new HashSet<string>();

            bool wordFound = false;
            while (subsetsOfLetters.Count > 0 && !wordFound)
            {
                foreach (string subsetOfLetters in subsetsOfLetters)
                {
                    hash = Hash(subsetOfLetters, charHashes);
                    if (AnagramDictionary.ContainsKey(hash))
                    {
                        word = AnagramDictionary[hash][0];// there might be multiple anagrams in this list but we only need one, so we take the first
                        wordFound = true;
                        break;
                    } 
                    else
                    {
                        letterSet = new HashSet<char>(subsetOfLetters.ToCharArray());
                        foreach (char letter in letterSet)
                        {
                            nextSubSets.Add(RemoveCharFromString(letter, subsetOfLetters));
                        }
                    }
                }
                subsetsOfLetters = new HashSet<string>(nextSubSets);
                nextSubSets.Clear();
            }

            return word;
        }

        static string RemoveCharFromString(char charToRemove, string str)
        {
            char[] output = new char[str.Length-1];
            bool charRemoved = false;
            for (int i=0; i < str.Length; i++)
            {
                char letter = str[i];
                if (charRemoved == true)
                {
                    output[i-1] = letter;
                }
                else if (letter != charToRemove)
                {
                    output[i] = letter;
                } 
                else
                {
                    charRemoved = true;
                }
            }
            return new string(output);
        }

        /// <summary>
        /// Asks user for consonant or vowel nine times, producing a sequence of letters which is then returned.
        /// </summary>
        static string GetLetters(Dictionary<char, float> letterFrequencies)
        {
            string letters = "";
            char letter;

            int counter = 0;
            while (counter < maxWordLength)
            {
                letter = GenerateLetter(GetBooleanResponse("Would you like a consonant or a vowel? (c/v)", 
                                                        "I didn't understand that. Please type c or v: ",
                                                        acceptedInputsMeaningConsonant, acceptedInputsMeaningVowel),
                                                        letterFrequencies);
                letters += letter;
                Console.WriteLine("Letters: " + letters);

                counter++;
            }
            return letters;
        }

        /// <summary>
        /// Gets a response from the user which falls into one of two given categories.
        /// <param name="message">Question to ask the player</param>
        /// <param name="invalidResponseMessage">Message for the player if their response is not in either category</param>
        /// <param name="trueResponses">If the user gives one of these responses, we return true</param>
        /// <param name="falseResponses">If the user gives one of these responses, we return false</param>
        /// </summary>
        static bool GetBooleanResponse(string message, string invalidResponseMessage, HashSet<string> trueResponses, HashSet<string> falseResponses)
        {
            string input;
            bool inputIsAcceptable = false;
            bool output = true;
            while (!inputIsAcceptable)
            {
                Console.WriteLine(message);
                input = Regex.Replace(Console.ReadLine(), @"\t|\n|\r", "");
                if (trueResponses.Contains(input))
                {
                    inputIsAcceptable = true;
                    output = true;
                }
                else if (falseResponses.Contains(input))
                {
                    inputIsAcceptable = true;
                    output = false;
                } else
                {
                    message = invalidResponseMessage;
                }
            }
            return output;
        }

        /// <summary>
        /// Returns a random letter from consonants or letters, with the probability of a letter being chosen being relative to its frequency
        /// <param name="isConsonant">True if we only pick from consonants, false for vowels</param>
        /// </summary>
        static char GenerateLetter (bool isConsonant, Dictionary<char, float> letterFrequencies)
        {
            char output = 'a';

            char[] letters;
            float frequencySum;
            if (isConsonant)
            {
                letters = new char[consonants.Count];
                consonants.CopyTo(letters);
                frequencySum = consonantFrequencySum;
            }
            else
            {
                letters = new char[vowels.Count];
                vowels.CopyTo(letters);
                frequencySum = vowelFrequencySum;
            }

            Random randomNumberGenerator = new Random();
            double randomNumberInRange = randomNumberGenerator.NextDouble() * frequencySum;

            //weight letter chance by its frequency in English Language
            float runningSum = 0;
            foreach (char letter in letters)
            {
                float frequency = letterFrequencies[letter];
                runningSum += frequency;
                if (runningSum > randomNumberInRange)
                {
                    output = letter;
                    break;
                }
            }
            return output;
        }

        /// <summary>
        /// Returns the address of the directory where all the project files are
        /// </summary>
        static string GetRootAddress()
        {
            string binAddress = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory);
            string directoryAddress = Directory.GetParent(Directory.GetParent(Directory.GetParent(binAddress).FullName).FullName).FullName;
            string slash = "\\";
            return directoryAddress + slash;
        }

        /// <summary>
        /// Adds contents of the anagram file to AnagramDictionary. There is no allowance for a corrupt file, 
        /// but if there is no file, GenerateAnagramFile is called. These contents are a translation from
        /// hash codes to their corresponding words. The hashing function is such that words that are anagrams 
        /// have the same hash code.
        /// <param name="rootAddress">The address of the directory where all the project files are</param>
        /// </summary>
        static Dictionary<double, List<string>> ReadAnagramFile(string rootAddress, Dictionary<char, int> charHashes)
        {
            string fileName = rootAddress + anagramDictionaryFilename;
            Dictionary<double, List<string>> AnagramDictionary;

            if (File.Exists(fileName))
            {
                AnagramDictionary = new Dictionary<double, List<string>>();

                Int32 BufferSize = 4096;
                using var fileStream = File.OpenRead(fileName);
                using var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, BufferSize);
                String line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    Tuple<double, List<string>> lineData = ParseAnagramFileLine(line);

                    AnagramDictionary[lineData.Item1] = lineData.Item2;
                }
            } 
            else
            {
                AnagramDictionary = GenerateAnagramFile(rootAddress, charHashes);
            }

            return AnagramDictionary;
        }

        /// <summary>
        /// Converts a line from the anagram file into the hash and list of words it contains
        /// </summary>
        static Tuple<double, List<string>> ParseAnagramFileLine(string line)
        {
            string strippedLine = Regex.Replace(line, @"\t|\n|\r", "");
            string[] lineData = strippedLine.Split(" ");
            double hash = double.Parse(lineData[0]);//first part is just one character

            List<string> words = new List<string>();
            for (int i=1; i < lineData.Length; i++)
            {
                words.Add(lineData[i]);
            }
            return new Tuple<double, List<string>>(hash, words);
        }

        /// <summary>
        /// Adds contents of the letter frequency file to letterFrequencies.
        /// Natural frequencies of letters in the english dictionary.
        /// <param name="rootAddress">The address of the directory where all the project files are</param>
        /// </summary>
        static Dictionary<char, float> ReadLetterFrequencies(string rootAddress)
        {
            string fileName = rootAddress + frequenciesFilename;

            Dictionary<char, float> letterFrequencies = new Dictionary<char, float>();

            Int32 BufferSize = 4096;
            using var fileStream = File.OpenRead(fileName);
            using var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, BufferSize);
            String line;
            while ((line = streamReader.ReadLine()) != null)
            {
                string data = Regex.Replace(line, @"\t|\n|\r", "");
                string[] datas = data.Split(" ");
                char c = datas[0].ToCharArray()[0];//first part is just one character
                float frequency = float.Parse(datas[1], CultureInfo.InvariantCulture.NumberFormat);
                letterFrequencies[c] = frequency;

                if (consonants.Contains(c))
                {
                    consonantFrequencySum += frequency;
                } else
                {
                    vowelFrequencySum += frequency;
                }
            }

            return letterFrequencies;
        }

        /// <summary>
        /// Assigns a prime number to each letter of the alphabet, in the dictionary charHashes
        /// </summary>
        static Dictionary<char, int> GenerateCharHashes()
        {
            Dictionary<char, int> charHashes = new Dictionary<char, int>();
            int counter = 0;
            foreach (char letter in alphabet)
            {
                charHashes.Add(letter, primes[counter]);
                counter++;
            }

            return charHashes;
        }

        /// <summary>
        /// Generates a file containing a translation from hash codes to their corresponding words. 
        /// The hashing function is such that words that are anagrams have the same hash code.
        /// It reads from the word list dictionaryFilename, and writes to anagramDictionaryFilename
        /// <param name="rootAddress">The address of the directory where all the project files are</param>
        /// </summary>
        static Dictionary<double, List<string>> GenerateAnagramFile(string rootAddress, Dictionary<char, int> charHashes)
        {
            string readFileName = rootAddress + dictionaryFilename;
            string writeFileName = rootAddress + anagramDictionaryFilename;

            Dictionary<double, List<string>> AnagramDictionary = new Dictionary<double, List<string>>();

            Int32 BufferSize = 4096;
            using var fileStream = File.OpenRead(readFileName);
            using var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, BufferSize);
            String line;
            while ((line = streamReader.ReadLine()) != null)
            {
                string word = Regex.Replace(line, @"\t|\n|\r", "").ToLower();
                if (word.Length <= maxWordLength)
                {
                    double hash = TryHash(word, charHashes);
                    if (hash != -1)
                    {
                        if (!AnagramDictionary.ContainsKey(hash))
                        {
                            AnagramDictionary[hash] = new List<string>();
                        }
                        AnagramDictionary[hash].Add(word);
                    }
                }
            }

            List<double> wordHashes = new List<double> (AnagramDictionary.Keys);
            wordHashes.Sort();

            using StreamWriter file = new StreamWriter(writeFileName);
            foreach (double wordHash in wordHashes)
            {
                string words = string.Join(" ", AnagramDictionary[wordHash]);
                file.WriteLine(wordHash.ToString() + " " + words);
            }

            return AnagramDictionary;
        }

        /// <summary>
        /// Returns the hash of word, or -1 if word contains characters not in charHashes.
        /// Since charHashes assigns a prime number to each letter, the hash of a word is the product of these primes.
        /// Hence anagrams have the same hash.
        /// <param name="word">The word to return the hash of</param>
        /// </summary>
        static double TryHash(string word, Dictionary<char, int> charHashes)
        {
            double product = 1;
            foreach (char c in word)
            {
                if (charHashes.ContainsKey(c))
                {
                    product *= charHashes[c];
                }
                else
                {
                    return -1;
                }
            }
            return product;
        }

        /// <summary>
        /// Returns the hash of word.
        /// Since charHashes assigns a prime number to each letter, the hash of a word is the product of these primes.
        /// Hence anagrams have the same hash.
        /// <param name="word">The word to return the hash of</param>
        /// </summary>
        static double Hash(string word, Dictionary<char, int> charHashes)
        {
            double product = 1;
            foreach (char c in word)
            {
                product *= charHashes[c];
            }
            return product;
        }
    }
}
